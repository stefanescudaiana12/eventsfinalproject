package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.repository.EventRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository){
        this.eventRepository = eventRepository;
    }

    public Event saveEvent(Event event){
        if(event == null){
            throw new IllegalArgumentException("An event must have body");
        }
        if(event.getName() ==  null) {
            //Avoid the generator of NullPointerException for null events name!
            throw new IllegalArgumentException("An argument must have a name!");
        }
        if(event.getStartDate() == null || event.getEndDate() == null || event.getStartDate().isAfter(event.getEndDate())){
            //Avoid the generator of NullPointerException for null dates!
            throw new IllegalArgumentException("Event start date is after its end date. Please be careful!");
        }
        Event savedEvent = eventRepository.save(event);
        return  savedEvent;
    }

    public Event readEvent(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("EventID must not be null");
        }
        Event event = eventRepository.findById(id).orElse(null);
        if(event == null){
            throw new IllegalArgumentException("There is not event with ID: " + id);
        }
        return event;
    }
    public List<Event> readAllEvents(){
        return eventRepository.findAll();
    }

     public Event updateEvent(Event updatedEvent){
         if(updatedEvent == null){
             throw new IllegalArgumentException("An event must have body");
         }
         //ca sa verificam ca exista un event cu id-ul dat in baza de date
         Event eventToUpdate = readEvent(updatedEvent.getId());
         //salvam toate verificarile de rigoare evenimentului in baza de date
         return saveEvent(updatedEvent);
     }

     public void deleteEvent(Integer eventId){
        Event eventToDelete = readEvent(eventId);
        eventRepository.delete(eventToDelete);
     }

}



