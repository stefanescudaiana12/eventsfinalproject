package ro.sda.eventsFinalProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sda.eventsFinalProject.model.Event;

public interface EventRepository extends JpaRepository<Event,Integer> {

}
